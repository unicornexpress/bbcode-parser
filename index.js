'use strict';

const parse = require('./parser');
const fs = require('fs');

fs.readFile('./test.txt', (err, data) => {
  if(!err) {
    let str = data.toString('utf-8');
    let p = parse(str);
    fs.writeFile('./out.json', JSON.stringify(p, null, 2), _ => console.log('Done!'));
  }
})

