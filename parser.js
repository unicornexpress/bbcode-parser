'use strict';

function type(type, child) {
  return {
    type: type,
    child: child || null
  }
}

function text(string) {
  return type('text', string);
}

function parseAttributes(string) {
  let str = string;

  const shift = () => {
    endAt++;
    let c = str[0];
    str = str.substr(1);
    return c;
  }

  let selfClosing = false;
  const attrs = {};
  let endAt = 0;
  
  let escapesCount = 0;
  let curAttr = '';
  let currVal = '';

  let parseValue = false;

  while(str.length) {
    let c = shift();

    if (parseValue) { // parsing value
      while(str.length && c !== '"' && escapesCount%2 !== 1) {
        if (c === '\\') {
          escapesCount++;
        }
        currVal += c;
        c = shift();
      }
      parseValue = false;
      attrs[curAttr] = currVal;
      curAttr = '';
      currVal = '';
    } else if(/[a-z]/.test(c)) { // test param name
      while(str.length && c !== ' ' && c !== '=' && escapesCount%2 !== 1) { // while param name not end
        if (c === '\\') {
          escapesCount++;
        }
        curAttr += c;
        c = shift();
      }
      if (! /^[a-z][a-zA-Z]+$/.test(curAttr)) {
        throw new Error(`Invalid attr name character "${curAttr}"`);
      } else { 
        parseValue = c === "=" && str[0] === '"';
        if(!parseValue) {
          attrs[curAttr] = true;
          curAttr = '';
        } else {
          c = shift();
        }
      }
    } else if(c === '/' && str[0] === ']') { // selfclosing tag
      selfClosing = true;
    } else if(c === ' ') {
      if (curAttr) {
        attrs[curAttr] = true;
        curAttr = '';
      }
    } else if (c === ']') {
      if(curAttr) {
        attrs[curAttr] = true;
      }
      parseValue = false;
      return {endAt, attrs: Object.keys(attrs).length ? attrs : null , selfClosing};      
    } else {
      throw new Error(`Invalid tag syntax near: "${str.substr(0, 10)}..."`);
    }
  }

  return {
    selfClosing,
    attrs,
    endAt
  };
}

function findTag(string, tag) {
  let str = string;
  const { selfClosing, attrs, endAt } = parseAttributes(str); // find attributes
  if (attrs) {
    tag.attrs = attrs;
  }
  if(selfClosing) {
    tag.selfClosing = selfClosing;
  }
  str = str.substr(endAt);

  if (! selfClosing) {
    const closingTag = `[/${tag.type}]`
    let closing = str.indexOf(closingTag);
    if (closing < 0) {
      throw new Error(`Missing closing tag [/${tag.type}]`)
    }
    tag.child = parser(str.substr(0, closing));
    str = str.substr(closing + closingTag.length);
  }
  return str;
}

const tagReg = /\[([a-z][a-zA-Z]*)/;

function parser(string) {

  let str = string; // stay immutable
  let list = []; // return collection

  while(str.length) {

    const found = str.match(tagReg); // find 1st tag  
    if(found) {
      const pos = found.index; 
      const match = found[0];
      const tag = type(found[1]);
      
      if (pos > 0) { // cut prefix text
        list.push(text(str.substr(0, pos)));
        str = str.substr(pos);
      }

      str = str.substr(match.length); // cut off tagname
      str = findTag(str, tag);

      list.push(tag);
    } else {
      list.push(text(str));
      str = '';
    }
  }
  
  return list;
}

module.exports = parser;